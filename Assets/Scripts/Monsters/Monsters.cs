﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monsters : Units
{
    protected Character target;
    new protected Collider2D collider;

    protected virtual void Awake()
    {
        collider = GetComponent<Collider2D>();
        target = transform.Find("/WorldSpace/Character").gameObject.GetComponent<Character>();
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        Character charact = collision.GetComponent<Character>();
        if (charact == target)
        {
            Atack(0.5F);
        }
    }

    protected virtual void Atack(float damage)
    {
        if(target != null)target.ResivedDamage(0.5F);
    }

    protected virtual void Move()
    {
        if (target != null)  transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);
    }


}
