using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private float speed = 5;

    public void CameraMove(float x, float y)
	{
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(x, y, 0), (speed * 10) * Time.deltaTime);
	}
}