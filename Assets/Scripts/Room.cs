﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    private CameraController cameraController;
    private Character chara;

    public int x;
    public int y;

    private bool characterInput = false;
    public bool CharacterInput
    {
        set { characterInput = value; }
    }

    public bool doorUpExist
    {
        set { transform.Find("Door_Up").gameObject.SetActive(value); }
    }
    public bool doorDownExist
    {
        set { transform.Find("Door_Down").gameObject.SetActive(value); }
    }
    public bool doorLeftExist
    {
        set { transform.Find("Door_Left").gameObject.SetActive(value); }
    }
    public bool doorRightExist
    {
        set { transform.Find("Door_Right").gameObject.SetActive(value); }
    }

    private void Awake()
    {
        cameraController = transform.Find("/WorldSpace/Camera").GetComponent<CameraController>();
        chara = transform.Find("/WorldSpace/Character").GetComponent<Character>();
    }

    private void Update()
    {
        if (characterInput && (cameraController.transform.position != transform.position))
        {
            cameraController.CameraMove(transform.position.x, transform.position.y);
        }
    }

    //struct roomType
    //{
    //    bool isBossRoom;
    //    bool isTreasherRoom;
    //    bool isRoom;

    //    public void RoomType(bool p1)
    //    {
    //        if (!isBossRoom || !isTreasherRoom) isRoom = p1;
    //        else { isBossRoom = false; isTreasherRoom = false; isRoom = p1; }

    //        if (!isRoom || !isTreasherRoom) isBossRoom = p1;
    //        else { isRoom = false; isTreasherRoom = false; isBossRoom = p1; }

    //        if (!isRoom || !isBossRoom) isTreasherRoom = p1;
    //        else { isRoom = false; isBossRoom = false; isTreasherRoom = p1; }
    //    }
    //}
}
