﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : Units
{

    private int luck;

    private float range;

    private Rigidbody2D bodyPhysic;

    private EdgeCollider2D headBodyCollider;

    private BoxCollider2D bodyCollider;

    private SpriteRenderer headSprite;

    private SpriteRenderer bodySprite;

    private bool isReloaded = true;

    private bool waitFlagCam;
    public bool WaitFlagCam
    {
        set { waitFlagCam = value; }
    }

    private void Awake()
    {
        bodyPhysic = GetComponentInChildren<Rigidbody2D>();
        headBodyCollider = GetComponent<EdgeCollider2D>();
        bodyCollider = GetComponent<BoxCollider2D>();
        headSprite = transform.Find("/WorldSpace/Character/Head").gameObject.GetComponent<SpriteRenderer>();
        bodySprite = transform.Find("/WorldSpace/Character/Body").gameObject.GetComponent<SpriteRenderer>();
        waitFlagCam = true;
        speed = 4;
        rate = 5;
        damage = 1.5F;
    }

    private void FixedUpdate()
    {
        if ((Input.GetButton("Horizontal") || Input.GetButton("Vertical")) && waitFlagCam) Move();
    }

    private void Update()
    {
        if ((Input.GetButton("FireHorizontal") || Input.GetButton("FireVertical")) && isReloaded) Shoot();
    }

    private void Move()
    {
        float diagonal = 1;
        Vector3 direction = new Vector3(0, 0, 0);
        direction.x += Input.GetAxis("Horizontal");
        direction.y += Input.GetAxis("Vertical");
        if (Input.GetButton("Horizontal") && Input.GetButton("Vertical")) diagonal = 0.85F;
        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, ((speed * 2) * diagonal) * Time.deltaTime);
    }

    private void Shoot()
    {
        Vector3 direction = new Vector3(0, 0, 0);
        direction.x += Input.GetAxis("FireHorizontal");
        direction.y += Input.GetAxis("FireVertical");

        Tear tear = Resources.Load<Tear>("Prefabs/Tear");
        Tear cloneTear = Instantiate<Tear>(tear, transform.position + new Vector3(0, 0.2F), Quaternion.identity);
        cloneTear.Parent = transform;
        cloneTear.Speed = rate;
        cloneTear.Direction = direction;
        cloneTear.Damage = damage;
        StartCoroutine(reloading());
    }

    private IEnumerator reloading()
    {
        isReloaded = false;
        yield return new WaitForSeconds(rate * 0.08F);
        isReloaded = true;
    }
}
