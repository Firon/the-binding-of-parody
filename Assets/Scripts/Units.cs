﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Units : MonoBehaviour
{

    protected float helth;

    protected float speed;

    protected int rate;

    protected float damage;

    public virtual void ResivedDamage(float damage)
    {
        helth -= damage;

        if (helth <= 0) Die();
    }

    protected virtual void Die()
    {
        Destroy(this.gameObject);
    }
}
