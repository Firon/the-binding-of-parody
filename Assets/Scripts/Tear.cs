﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tear : MonoBehaviour
{
    new private Collider2D collider;

    private Vector3 direction;
    public Vector3 Direction
    {
        set { direction = value; }
    }

    private float damage;
    public float Damage
    {
        set { damage = value; }
    }

    private float speed;
    public float Speed
    {
        set { speed = value; }
    }

    private Transform parent;
    public Transform Parent
    {
        set { parent = value; }
    }

    private void Awake()
    {
        collider = GetComponent<Collider2D>();
    }

    private void FixedUpdate()
    {
        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collider2D)
    {
        Units unit = collider2D.GetComponent<Units>();
        if (unit && unit.transform != parent && unit is Monsters)
        {
            unit.ResivedDamage(damage);
            Destroy(this.gameObject);
        }
        else if (!unit) Destroy(this.gameObject);
    }
}
