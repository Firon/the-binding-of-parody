﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    new private Collider2D collider;
    private Map map;
    private Room room;

    private void Start()
    {
        room = transform.parent.GetComponent<Room>();
        collider = GetComponent<Collider2D>();
        map = transform.Find("/WorldSpace").GetComponent<Map>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Character chara = collision.GetComponent<Character>();

        if (chara)
        {
            Transport(chara);
        }
    }

    public void Transport(Character chara)
    {
        if (name == "Door_Up")
        {
            chara.transform.position = new Vector3(chara.transform.position.x, chara.transform.position.y + 9, 2);
            map.CharacterMove("Up");
        }
        else if (name == "Door_Down")
        {
            chara.transform.position = new Vector3(chara.transform.position.x, chara.transform.position.y - 9, 2);
            map.CharacterMove("Down");
        }
        else if (name == "Door_Left")
        {
            chara.transform.position = new Vector3(chara.transform.position.x - 13, chara.transform.position.y, 2);
            map.CharacterMove("Left");
        }
        else if (name == "Door_Right")
        {
            chara.transform.position = new Vector3(chara.transform.position.x + 13, chara.transform.position.y, 2);
            map.CharacterMove("Right");
        }
    }
}
