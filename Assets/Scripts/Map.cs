﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Map : MonoBehaviour
{
    private const int maxMapSize = 12;
    private const int minMapSize = 0;
    private const int midMapSize = (maxMapSize - minMapSize) / 2;
    private int maxCountRoom;
    private int countRoom = 1;
    private int maxNumRoom = 1;
    private int direction;
    private int lustDirection = 0;

    private Room[,] map = new Room[maxMapSize, maxMapSize];
    private Marker marker;

    private Marker charaMarker;

    private void Awake()
    {
        StartGeneration();
    }

    private bool BorderCheck(Marker mark)
    {
        return minMapSize < mark.x && mark.x < maxMapSize && minMapSize < mark.y && mark.y < maxMapSize;
    }

    public void CharacterMove(string direction)
    {
        if (direction == "Up")
        {
            map[charaMarker.x, charaMarker.y].CharacterInput = false;
            charaMarker.y--;
            map[charaMarker.x, charaMarker.y].CharacterInput = true;
        }
        else if (direction == "Down")
        {
            map[charaMarker.x, charaMarker.y].CharacterInput = false;
            charaMarker.y++;
            map[charaMarker.x, charaMarker.y].CharacterInput = true;
        }
        else if (direction == "Left")
        {
            map[charaMarker.x, charaMarker.y].CharacterInput = false;
            charaMarker.x++;
            map[charaMarker.x, charaMarker.y].CharacterInput = true;
        }
        else if (direction == "Right")
        {
            map[charaMarker.x, charaMarker.y].CharacterInput = false;
            charaMarker.x--;
            map[charaMarker.x, charaMarker.y].CharacterInput = true;
        }
    }

    private void StartGeneration()
    {
        marker.y = midMapSize;
        marker.x = midMapSize;
        maxCountRoom = Random.Range(10, 16);
        countRoom = 1;

        GameObject newMap = new GameObject();
        newMap.name = "Map";
        newMap.transform.parent = transform;
        Debug.Log(midMapSize);
        // Создает первую комнату на этаже.
        Room room = Resources.Load<Room>("Prefabs/Rooms/Room_1");
        map[marker.x, marker.y] = Instantiate<Room>(room, new Vector3(0, 0, 2), Quaternion.identity);
        map[marker.x, marker.y].transform.parent = transform.Find("/WorldSpace/Map").transform;
        map[marker.x, marker.y].x = midMapSize - marker.x;
        map[marker.x, marker.y].y = midMapSize - marker.y;
        map[marker.x, marker.y].CharacterInput = true;
        charaMarker.x = midMapSize;
        charaMarker.y = midMapSize;
        
        do MoveGeneration(); while (countRoom <= maxCountRoom);

        CreateDoors();
    }

    private void CreateDoors()
    {
        for (int x = 0; x < maxMapSize - 1; x++)
        {
            for (int y = 0; y < maxMapSize - 1; y++)
            {
                if (map[x, y] != null)
                {
                    if (map[x + 1, y] != null) map[x, y].doorLeftExist = true; 
                    if (map[x - 1, y] != null) map[x, y].doorRightExist = true;
                    if (map[x, y + 1] != null) map[x, y].doorDownExist = true;
                    if (map[x, y - 1] != null) map[x, y].doorUpExist = true;
                }
            }
        }
    }

    private void MoveGeneration()
    {
        do direction = Random.Range(1, 5); while (direction == lustDirection);
        lustDirection = direction;
        int Count = Random.Range(2, 5);
        for (int i = 0; i < Count; i++)
        {
            if (countRoom <= maxCountRoom)
            {
                switch (direction)
                {
                    case 1: marker.x++; break;
                    case 2: marker.y++; break;
                    case 3: marker.x--; break;
                    case 4: marker.y--; break;
                }
                CreatRoom(marker);
            }
        }
    }

    private void CreatRoom(Marker cell)
    {
        if (BorderCheck(cell))
        {
            
            if (map[cell.x, cell.y] == null)
            {
                countRoom++;
                Room room = Resources.Load<Room>("Prefabs/Rooms/Room_" + Random.Range(1, maxNumRoom));
                Room cloneRoom = Instantiate<Room>(room, new Vector3(0, 0, 0), Quaternion.identity);
                cloneRoom.transform.parent = transform.Find("/WorldSpace/Map").transform;
                cloneRoom.transform.position = new Vector3((midMapSize - marker.x) * 33, (midMapSize - marker.y) * 20, 2);
                map[marker.x, marker.y] = cloneRoom;
            }
        }
        else ReturnMark();
    }

    private void ReturnMark()
    {
        
        marker.y = midMapSize;
        marker.x = midMapSize;
    }

    public struct Marker
    {
        public int x;
        public int y;

        public void marker(int p)
        {
            x = p;
            y = p;
        }
    }
}
